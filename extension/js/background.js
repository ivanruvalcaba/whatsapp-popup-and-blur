/*
 * File: background.js
 *
 * Created: 20 jun 2018 10:04:59
 * Last Modified: 20 jun 2018 12:48:49
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

browser.browserAction.onClicked.addListener((tab) => {  // eslint-disable-line no-unused-vars
  const win_whastapp = browser.windows.create({ // eslint-disable-line no-unused-vars
    url: ['https://web.whatsapp.com/'],
    type: 'panel',
    height: 520,
    width: 660
  });
});
