/*
 * File: content.js
 *
 * Created: 20 jun 2018 12:59:14
 * Last Modified: 21 jun 2018 12:06:49
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

/*eslint no-undef: off*/

addFilter = () => {
  blurImages();
  blurVideos();
  blurAvatars();
};

addClass = els => {
  [].forEach.call(els, el => {
    el.classList.add('noblur');
  });
};

blurVideos = () => {
  const els = document.querySelectorAll('.tail video, .message-in  video, .message-out video');

  if(els.length) {
    [].forEach.call(els, el => {
      el.parentNode.classList.add('noblur');
      el.style.webkitFilter = 'blur(30px)';
    });
  }
};


blurImages = () => {
  const els = document.querySelectorAll('.tail img:not(.copyable-text), .message-in img:not(.copyable-text), .message-out img:not(.copyable-text), .tail div[style*=background-image], .message-in div[style*=background-image], .message-out div[style*=background-image]');

  if(els.length) {
    [].forEach.call(els, el => {
      el.parentNode.parentNode.classList.add('noblur');
      el.style.webkitFilter = 'blur(30px)';
    });
  }
};

blurAvatars = () => {
  const els = document.querySelectorAll('span[data-icon=default-group]');

  if(els.length) {
    [].forEach.call(els, el => {
      el.parentNode.parentNode.classList.add('noblur');
      let img = el.parentNode.parentNode.querySelector('img');
      img.style.webkitFilter = 'blur(30px)';
    });
  }
};

setInterval(() => {
  addFilter();
}, 500);
