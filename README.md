[//]: # (Filename: README.md)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <mario.i.ruvalcaba[at]gmail[dot]com>)
[//]: # (Created: 21 jun 2018 08:54:41)
[//]: # (Last Modified: 21 jun 2018 12:07:39)

# Whatsapp popop & blur

<center>
![](doc/img/whatsapp-blur.png)
</center>

> A minimal Whatsapp popup with Blur build-in functionality — Mozilla Firefox extension

This Firefox extension allows you to open a pop-up window ([Whatsapp Web](https://web.whatsapp.com/)) in addition to protecting you from prying eyes by automatically blurring all types of images, videos and icons that your contacts send you.

See it in action:

![](doc/img/whatsapp-popup-and-blur.png)

## Development

**First step:** Fork it:

```sh
git clone https://gitlab.com/ivanruvalcaba/whatsapp-popup-and-blur.git
```

**Second step:** Install all project dependecies/tools (web development only):

> Replace `yarn` with `npm` if is your case.

```sh
yarn install
```

Then run:

+ `yarn start:firefox`: Launch Firefox in dev mode, builds and then temporarily installs an extension, so it can be tested. By default, watches extension source files and reload the extension in each target as files change.
+ `yarn lint`: Reports errors in the _extension manifest_ or other source code files.
+ `yarn build`: Packages the extension into a `.zip` file, ignoring files that are commonly unwanted in packages, such as `.git` and other artifacts. The name of the `.zip` file is taken from the name field in the _extension manifest_.

Enjoy!!!

## Licence

Mozilla Public License Version 2.0
